<?php

class Nullor_Orderclean_Model_Observer
{
    /**
     * 自动取消超过 90 分钟未付款的订单
     */
    public function cancelPendingOrders()
    {
            $orderCollection = Mage::getResourceModel('sales/order_collection');
 
            $orderCollection
                    ->addFieldToFilter('state', 'pending_payment')
                    ->addFieldToFilter('created_at', array('lt' =>  new Zend_Db_Expr("DATE_ADD('".now()."', INTERVAL -'90:00' HOUR_MINUTE)")))
                    ->getSelect()
                    ->order('e.entity_id')
                    ->limit(10)                  
            ;
 
 
           $orders ="";
            foreach($orderCollection->getItems() as $order)
            {
              $orderModel = Mage::getModel('sales/order');
              $orderModel->load($order['entity_id']);
 
 
              if(!$orderModel->canCancel())
                continue;
 
              $orderModel->cancel();
              $orderModel->setStatus('canceled_pendings');
              $orderModel->save();
 
            }
    }
}