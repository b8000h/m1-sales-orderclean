

Automatically cancel pending orders older than 90 minutes
 
There are situations where you might with to automatically cancel orders older than 90 minutes. 
They stay in “pending_payment” state. 
This usually happen when the customer didn’t complete the checkout due any reason.
 
We created observer class “Inchoo_Order_Model_Observer” with public method “cancelPendingOrders“.
We are calling this method over cron script, take a look at our config.xml file below and everting will be clear.
In this method we filtered orders which have state “pending_payment” and older than 90 minutes. 

Also we set limit to 10 orders, because we want to process them in one loop.
Before save order we set new order state to “canceled_pendings“. 
All canceled orders will have new state “canceled_pendings“.